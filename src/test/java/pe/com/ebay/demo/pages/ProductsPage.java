package pe.com.ebay.demo.pages;

import java.util.List;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import pe.com.ebay.demo.base.TestBase;

public class ProductsPage extends TestBase {
	
	 Actions builder = new Actions(driver);

	@FindBy(css=".rcnt")
	List<WebElement> resultsFilter;
	
	@FindBy(css=".srp-controls__count-heading")
	List<WebElement> resultsFilter1;
	
	@FindBy(css=".sort-menu-container .dropdown-toggle")
	List<WebElement> dropDownFilter;
	
	@FindBy(css=".srp-controls--selected-value")
	List<WebElement> dropDownFilter1;
	
	@FindBy(css="[data-pos='1'] [value='15']")
	List<WebElement> orderlowestFirst;
	
	@FindBy(css=".srp-sort__menu > li:nth-child(4) > a > span")
	List<WebElement> orderlowestFirst1;
	
	@FindBy(linkText="Price + Shipping: lowest first")
	WebElement orderlowestFirst2;
	
	@FindBy(css=".sm-md [href]")
	WebElement vieAllResults;
	
	@FindBy(css=".amt span")
	List<WebElement> resultsFirstPage;
	
	@FindBy(css=".s-item__price")
	List<WebElement> resultsFirstPage1;
	
	@FindBy(css="h3 .vip")
	List<WebElement> nameProducts;
	
	@FindBy(css="h3[role='text']")
	List<WebElement> nameProducts1;

	// Initializing the Page Objects:
	public ProductsPage() {
		PageFactory.initElements(driver, this);
	}
	
	//Actions:
	public String validateLoginPageTitle(){
		return driver.getTitle();
	}
	
	public String getResults() {
		
		String numberResults= " " ;
		
		Boolean isPresent=resultsFilter.size() > 0;
		
		if(isPresent){
			numberResults=resultsFilter.get(0).getText();
		} else{
			
			numberResults=resultsFilter1.get(0).getText();
		}
		
		return numberResults;
	}
	
	public void clickOrderPriceAscendent() {
		
		orderlowestFirst2.click();
		
//		Boolean isPresent=orderlowestFirst1.size() > 0;
//		
//		if(isPresent){
//			orderlowestFirst1.get(0).click();
//		} else{
//			
//			orderlowestFirst.get(0).click();
//		}
		
	}
	
	public void selectDropDownElement() {
		
		Boolean isPresent=dropDownFilter.size() > 0;
		
		if(isPresent){
			builder.moveToElement(dropDownFilter.get(0)).build().perform();
		} else{
			
			builder.moveToElement(dropDownFilter1.get(0)).build().perform();
		}
		
	}
	
	public void orderPriceAscendent(){
		
		selectDropDownElement();
		clickOrderPriceAscendent();
		
	}
	
	public List<WebElement> getPriceProducts(){
		
		Boolean isPresent=resultsFirstPage.size() > 0;
		
		if (isPresent) {
			return resultsFirstPage;
		} else {
			return resultsFirstPage1;
		}
		
	}
	
	
	public List<WebElement> getNameProducts(){
		
		Boolean isPresent=nameProducts.size() > 0;
		
		if (isPresent) {
			return nameProducts;
		} else {
			return nameProducts1;
		}
		
	}
}
