package pe.com.ebay.demo.pages;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import pe.com.ebay.demo.base.TestBase;
import pe.com.ebay.demo.util.TestUtil;

public class CategoryPage extends TestBase {
	
	TestUtil testutil=new TestUtil();

	@FindBy(id="w3-w0-w2-w2-0[0]")
	WebElement inputSearchBrand1;
	
	@FindBy(css="#e1-40")
	WebElement inputSearchBrand2;
	
	@FindBy(css="input[aria-label='adidas']")
	WebElement checkBoxBrand1;
	
	@FindBy(css="input[aria-label='PUMA']")
	WebElement checkBoxBrand2;
	
	@FindBy(xpath="//span[text()='PUMA']")
	WebElement checkBoxBrand3;
	
	@FindBy(css="input[aria-label='New with tags']")  //Mejorar la localizacion 
	WebElement checkBoxCondition1;
	
	// Initializing the Page Objects:
	public CategoryPage() {
		PageFactory.initElements(driver, this);
	}
	
	//Actions:
	public String validateLoginPageTitle(){
		return driver.getTitle();
	}
	
	public void selectBrand(String brand) {
		   
		testutil.waitForClickeable(checkBoxBrand3);
		TestUtil.waitFor();
		checkBoxBrand3.click();
		
		testutil.waitForClickeable(checkBoxCondition1); 
		TestUtil.waitFor();
		checkBoxCondition1.click();
	}
	
}
