package pe.com.ebay.demo.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import pe.com.ebay.demo.base.TestBase;

public class HomePage extends TestBase {

	@FindBy(id="gh-ac")
	WebElement inputSearch;

	@FindBy(id="gh-btn")
	WebElement buttonSearch;
	
	@FindBy(css="[data-imp] [role='button'] .gh-eb-Geo-flag")
	WebElement flagGeo;
	
	
	@FindBy(css="#gh-eb-Geo-a-en .gh-eb-Geo-txt")
	WebElement switchEnglish;
	
	// Initializing the Page Objects:
	public HomePage() {
		PageFactory.initElements(driver, this);
	}
	
	//Actions:
	public String validateLoginPageTitle(){
		return driver.getTitle();
	}
	
	public void inicio(String product) {
		flagGeo.click();
		switchEnglish.click();
		inputSearch.sendKeys(product);
        //buttonSearch.click();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", buttonSearch);
	}
	
}
