package pe.com.ebay.demo.testcases;

import org.testng.Assert;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pe.com.ebay.demo.base.TestBase;
import pe.com.ebay.demo.pages.HomePage;
import pe.com.ebay.demo.pages.CategoryPage;
import pe.com.ebay.demo.pages.ProductsPage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;

public class SearchShoes extends TestBase {

	final static Logger logger = Logger.getLogger(SearchShoes.class);

	HomePage homePage;
	CategoryPage categoryPage;
	ProductsPage productsPage;

	public SearchShoes() {
		super();
	}

	@BeforeMethod
	public void setUp() {
		initialization();
		homePage = new HomePage();
		categoryPage = new CategoryPage();
		productsPage = new ProductsPage();

	}

	// invocationCount = 3
	@Test(priority = 1)
	public void verifyPumaProducts() {

		String title = homePage.validateLoginPageTitle();
		Assert.assertEquals(title, "Electronics, Cars, Fashion, Collectibles, Coupons and More | eBay");

		homePage.inicio(prop.getProperty("product"));
		categoryPage.selectBrand(prop.getProperty("brand"));
		String mensaje1 = productsPage.getResults();

		// 5-Número de resultados luego de filtros

		logger.info("Número de productos" + " " + mensaje1);

		productsPage.orderPriceAscendent();

		List<WebElement> precios = productsPage.getPriceProducts();

		List<String> array_precios = new ArrayList<>();

		List<WebElement> nombres = productsPage.getNameProducts();

		List<String> array_nombres = new ArrayList<>();

		Iterator<WebElement> iterPrecios = precios.iterator();

		Iterator<WebElement> iterNombres = nombres.iterator();

		String nombrePrecio = " ";

		for (int i = 0; i < 5; i++) {

			WebElement nombreP = iterNombres.next();
			WebElement precioP = iterPrecios.next();

			nombrePrecio = nombrePrecio + "\n" + nombreP.getText().toString() + "|" + precioP.getText().toString();

			array_precios.add(precios.get(i).getText().substring(3));
			array_nombres.add(nombres.get(i).getText());

		}

		// 8-Mostrar los 5 primeros productos con sus precios

		logger.info("5 Primeros productos con sus precios:" + "\n" + nombrePrecio);

		// 9-Mostrar Nombres de Producto en orden ascendente
		System.out.println("**Nombres de Producto en orden ascendente**");

		Collections.sort(array_nombres);

		for (int i = 0; i < array_nombres.size(); i++) {

			System.out.println(array_nombres.get(i));
		}

		// 10-Mostrar Nombres de Producto en orden descendente
		System.out.println("**Precios en orden descendente**");

		Collections.sort(array_precios, Collections.reverseOrder());

		for (int i = 0; i < array_nombres.size(); i++) {

			System.out.println(array_nombres.get(i) + array_precios.get(i));
		}
	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}

}
