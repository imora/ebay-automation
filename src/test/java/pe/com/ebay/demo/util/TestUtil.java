package pe.com.ebay.demo.util;

import java.io.File;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.apache.log4j.Logger;
import pe.com.ebay.demo.base.TestBase;

public class TestUtil extends TestBase{
	
	public static long PAGE_LOAD_TIMEOUT = 20;
	public static long IMPLICIT_WAIT = 30;
	private static int sleepTime = 1;
    public static void setSleepTime( int time ){ sleepTime = time; }
    
	public static String TESTDATA_SHEET_PATH = "/Users/ivan/Documents/workspace"
			+ "/data/src/main/java/com/qa/testdata/datamock.xlsx";
	
	static Workbook book;
	static Sheet sheet;
	
	
	public void switchToFrame(){
		driver.switchTo().frame("mainpanel");
	}
	
	public static Object[][] getTestData(String sheetName) {
		FileInputStream file = null;
		try {
			file = new FileInputStream(TESTDATA_SHEET_PATH);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			book = WorkbookFactory.create(file);
		} catch (InvalidFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		sheet = book.getSheet(sheetName);
		Object[][] data = new Object[sheet.getLastRowNum()][sheet.getRow(0).getLastCellNum()];
		// System.out.println(sheet.getLastRowNum() + "--------" +
		// sheet.getRow(0).getLastCellNum());
		for (int i = 0; i < sheet.getLastRowNum(); i++) {
			for (int k = 0; k < sheet.getRow(0).getLastCellNum(); k++) {
				data[i][k] = sheet.getRow(i + 1).getCell(k).toString();
				// System.out.println(data[i][k]);
			}
		}
		return data;
	}
	
	public static void takeScreenshotAtEndOfTest() throws IOException {
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		String currentDir = System.getProperty("user.dir");
		
		FileUtils.copyFile(scrFile, new File(currentDir + "/screenshots/" + System.currentTimeMillis() + ".png"));
		
		}
	
	//Espera implicita
	public static void waitFor(){
	       try {
	          Thread.sleep(sleepTime * 1000L);
	       } catch (Exception e) {
	          System.out.println(e.toString());
	       }
	    }
	
	
	//Espera explicita elemento visible
	public void waitForVisibility(WebElement element) throws Error{
        new WebDriverWait(driver, 60)
             .until(ExpectedConditions.visibilityOf(element));
 }
	
	//Espera explicita elemento clickeable
	public void waitForClickeable(WebElement element) throws Error{
        new WebDriverWait(driver, 30)
             .until(ExpectedConditions.elementToBeClickable(element));
 }
	
	//Espera explicita elemento visible
	public void waitForVisibilityAll(WebElement element) throws Error{
        new WebDriverWait(driver, 60)
             .until(ExpectedConditions.elementToBeClickable(element));
 }
	
	
//	   public <T> String readText (T elementAttr) {
//	        if(elementAttr.getClass().getName().contains("By")) {
//	            return driver.findElement((By) elementAttr).getText();
//	        } else {
//	            return ((WebElement) elementAttr).getText();
//	        }
//	    }
//	

	

}
